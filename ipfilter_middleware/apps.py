from django.apps import AppConfig


class IpfilterConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ipfilter'
