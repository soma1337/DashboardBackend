# import socket

# from django.conf import settings
# from django.core.exceptions import PermissionDenied


class IpFilterMiddleware:
    # Only called once
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    # def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        # allowed_ips = settings.IPFILTER['ALLOWED_IPS']
        # client_ip = request.META.get('REMOTE_ADDR')
        # try:
        #     host_ip = socket.gethostbyname(
        #         'dashboardfrontend-agdj.onrender.com')
        # except socket.gaierror:
        #     host_ip = None

        # if client_ip not in allowed_ips and client_ip != host_ip:
        #     raise PermissionDenied

        # if  client_ip not in allowed_ips:
        #     raise PermissionDenied

        # response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.
        # response['X-IP-FILTER'] = "IP FILTER"
        # return response
